import React from 'react';
import { Card, Image, Icon, Button, Modal, Form } from 'semantic-ui-react';
import { textLocation } from '../Services/yelp'

class ResultTile extends React.Component {

  componentDidMount() {
    this.setState({
      business: this.props.resultCard
    })
  }

  handleTextLocation = (event) => {
    let loc = this.state.business.location
    textLocation(event.target.value, this.state.business.name, loc.address1, loc.city, loc.state, loc.zip_code, this.state.business.url)
  }

  render() {

    return(
        <Card style={{marginLeft: "5px", marginRight: "5px", marginBottom:"10px"}} stackable="true">
          <Image src={this.props.resultCard.image_url}/>
          <Card.Content>
            <Card.Header>
              {this.props.resultCard.name}
            </Card.Header>
            <Card.Meta>
              <span className='date'>
                Rating: {this.props.resultCard.rating}
              </span>
            </Card.Meta>
            <Card.Description>
              {this.props.resultCard.location.address1}
              <br/>
              {this.props.resultCard.location.city}, {this.props.resultCard.location.state} {this.props.resultCard.location.zip_code}
              <br/>
              {this.props.resultCard.display_phone}
              <br/><br/>

            </Card.Description>
          </Card.Content>
          <Card.Content extra>
            <a href={this.props.resultCard.url} target="_blank" rel="noopener noreferrer">
              <Icon name='external' />
              Visit Yelp Page
            </a>
          </Card.Content>
        </Card>
    )
  }
}

export default ResultTile


// <Modal trigger={<Button color="red">Text me this info!</Button>}>
// <Modal.Content>
// <Form onSubmit={this.handleTextLocation}>
// <Form.Input type='text' placeholder='Please enter your phone number'/>
// <Button type='submit'>Submit</Button>
// </Form>
// </Modal.Content>
// </Modal>
