import React from 'react';
import { Form, Dropdown, Button, Input } from 'semantic-ui-react';
import { exploreNeighborhood } from '../Services/yelp'
import ResultsContainer from './Results'

var categories = require('../categories.json');

class Explore extends React.Component {

  constructor() {
    super()
    this.state = {
      categories: null,
      results: null
    }
  }

  componentDidMount() {
    let subcategoriesNum = Object.keys(categories).length
    let catsArr = []
    for (let i = 0; i < subcategoriesNum; i++) {
      let catTitle = categories[i].title
      let eachCat = {
        "key": i,
        "text": catTitle,
        "value": categories[i]["alias"]
      }
      catsArr.push(eachCat)
    }
    this.setState({
      categories: catsArr,
      searchTerm: null,
      location: null,
      cat: null
    })
  }

  handleSubmit = (event) => {
    event.preventDefault()
    exploreNeighborhood(this.state.searchTerm, this.state.location, this.state.cat)
      .then((json) => {
        this.setState({
          results: json
        })
      })
  }

  handleSearchTerm = (event) => {
    this.setState({
      searchTerm: event.target.value
    })
  }

  handleLocation = (event) => {
    this.setState({
      location: event.target.value
    })
  }

  handleCat = (event) => {
    this.setState({
      cat: event.target.value
    })
  }

  render() {
    if (this.state.results === null) {
      return (
        <div style={{marginLeft:"30px", marginRight:"30px"}}>
        <Form onSubmit={this.handleSubmit} style={{marginLeft:"80px", marginRight:"80px"}}>
        <Form.Field required width={16}>
            <label>Search Term:</label>
            <Input onChange={this.handleSearchTerm} placeholder="Katz's Deli, Mary Mac's Tea Room, brunch, LES" />
          </Form.Field>
          <Form.Field required width={16}>
            <label>Location:</label>
            <Input onChange={this.handleLocation} placeholder="City, state, OR zipcode"/>
          </Form.Field>
        <Form.Dropdown label="Category: " placeholder="Required: Enter a category" fluid search selection options={this.state.categories} onChange={this.handleCat}/>
          <Button type='submit'>Submit</Button>
        </Form>
        </div>
      )
    } else {
      return (
        <div>
          <Form onSubmit={this.handleSubmit} style={{marginLeft:"80px", marginRight:"80px"}}>
            <Form.Input type="text" onChange={this.handleSearchTerm} placeholder="Required: Enter a search term (ex: Katz's Deli, Mary Mac's Tea Room, brunch, LES)"/>
            <Form.Input type="text" onChange={this.handleLocation} placeholder="Required: Enter a city, state, OR zipcode"/>
            <Dropdown placeholder="Required: Enter a category" fluid search selection options={this.state.categories} onChange={this.handleCat}/>
            <br/>
            <Button type='submit'>Search!</Button>
          </Form>
          <br/><br/>
          Here's something to get you started!
          <br/><br/>
          <ResultsContainer results={this.state.results.businesses}/>
        </div>
      )
    }
  }
}

export default Explore;
