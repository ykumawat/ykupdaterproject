import React from 'react';
import ResultTile from './ResultTile'
import { Card } from 'semantic-ui-react';


class Results extends React.Component {
  
  render() {
    const resultTiles = this.props.results.map((result, i) => {
      return (
          <ResultTile key={i} resultCard={result} />
      )
    })
    return (
      <Card.Group itemsPerRow={4} style={{textAlign:"center", marginLeft:"20px", marginBottom: "40px"}}>
        {resultTiles}
      </Card.Group>
    )
  }
}

export default Results;