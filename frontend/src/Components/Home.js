import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
import MainDisplay from './MainDisplay';


class Home extends React.Component {
  render() {
    return (
      <div>
        <h3 style={{fontFamily:"maquette, Helvetica Neue, Helvetica, Arial, sans-serif"}}>Moving? Get to know your new neighborhood!</h3>
        <MainDisplay/>
      </div>
    )
  }
}

export default Home;
