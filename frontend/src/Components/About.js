import React from 'react';

class About extends React.Component {
  
  render() {
    return(
      <div>
        <p style={{textAlign:"center", fontSize:"1.25em", fontFamily:"maquette, Helvetica Neue, Helvetica, Arial, sans-serif", paddingLeft: "5px", paddingRight: "5px"}}>Hi there!</p>
        <p style={{paddingBottom:"20px", textAlign:"left", paddingLeft:"10px"}}>
          * This web app was made with Ruby on Rails on the backend and uses PostgreSQL as a database.
          <br/>
          * The frontend uses React.js and, for styling, react-bootstrap and SemanticUI.
          <br/>
          * AWS node.js lambda function and API Gateway used to get around CORS issues in accessing the Yelp API.
          <br/>
          * All fonts and colors are styled à la Updater (except for the olive green navbar, I just like the olive green).
          <br/>
          * For more information on the sources of the photos in the carousel, inspect those elements.
          <br/>
          (alt tags may or may not include the reasons why I love those places)
          <br/>
          * The code for this project can be found here: <a target="_blank" rel="noopener noreferrer" href='https://bitbucket.org/ykumawat/ykupdaterproject'>Click Me!</a>
          <br/>
          * Please enjoy exploring your new neighborhood, old neighborhood, or just learning more about a new state!
          <br/>
          * For questions, please send me a message through my website contact form! <a target="_blank" rel="noopener noreferrer" href='http://www.ykumawat.me'>ykumawat.me</a>
          <br/>
          Or just email me ... if you have a link to this website, you probably have my email...
        </p>
      </div>
    )
  }
}

export default About;