import React from 'react'
import { Menu } from 'semantic-ui-react'
import { NavLink } from 'react-router-dom'

class Nav extends React.Component {

  render() {
    return(
        <Menu color={'olive'} inverted widths={3}>
          <Menu.Item>
            <NavLink activeClassName="active" to="/" style={{fontFamily:"maquette, Helvetica Neue, Helvetica, Arial, sans-serif"}}>Home</NavLink>
          </Menu.Item>
          <Menu.Item>
          <NavLink activeClassName="active" to="/explore" style={{fontFamily:"maquette, Helvetica Neue, Helvetica, Arial, sans-serif"}}>Explore</NavLink>
          </Menu.Item>
          <Menu.Item>
            <NavLink activeClassName="active" to="/about" style={{fontFamily:"maquette, Helvetica Neue, Helvetica, Arial, sans-serif"}}>About</NavLink>
          </Menu.Item>
        </Menu>
    )
  }
}

export default Nav;
