import React from 'react';
import { Carousel } from 'react-bootstrap';

class MainDisplay extends React.Component {
  render() {
    return(
      <Carousel style={{marginBottom: "50px"}}>
        <Carousel.Item>
          <img style={{border: "double", height:"500px", display: "block", margin: "0px auto"}} alt='NYC skyline' src='https://media.timeout.com/images/102596453/image.jpg'/>
        </Carousel.Item>
        <Carousel.Item>
          <img style={{border: "double", height:"500px", display: "block", margin: "0px auto"}} alt='Shack Shake for days' src='https://upload.wikimedia.org/wikipedia/commons/c/c2/Shake_Shack_Madison_Square.jpg'/>
        </Carousel.Item>
        <Carousel.Item>
          <img style={{border: "double", height:"500px", display: "block", margin: "0px auto"}} alt='Owls Head Park aka childhood' src='https://onthegrid.city/imager/s3_amazonaws_com/onthegrid.city/assets/grid/brooklyn/bay-ridge/owls-head-park/IMG_6467-1_958cc5e0ce9e8b61b7d8ceb036d1f79e.jpg'/>
        </Carousel.Item>
        <Carousel.Item>
          <img style={{border: "double", height:"500px", display: "block", margin: "0px auto"}} alt='Fidi! Shoutout Flatiron School' src='http://www.artisnewyork.com/wp-content/uploads/2016/08/financial_battery.jpg'/>
        </Carousel.Item>
        <Carousel.Item>
          <img style={{border: "double", height:"500px", display: "block", margin: "0px auto"}} alt='Chelsea galleries' src='https://i.pinimg.com/originals/c8/7b/26/c87b2648a8873e4629cce2059e573076.jpg'/>
        </Carousel.Item>
        <Carousel.Item>
          <img style={{border: "double", height:"500px", display: "block", margin: "0px auto"}} alt='Yay Union Square and Strand and Updater' src='https://s3-media4.fl.yelpcdn.com/bphoto/fi-Iezn0TMnMWGQn61TXaA/o.jpg'/>
        </Carousel.Item>
      </Carousel>
    )
  }
}

export default MainDisplay;