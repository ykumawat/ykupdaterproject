import React, { Component } from 'react';
import './App.css';
import Nav from './Components/Nav';
import AboutComponent from './Components/About';
import Home from './Components/Home';
import { Route } from 'react-router-dom';
import Explore from './Components/Explore'


class App extends Component {
  render() {
    return (
      <div className="App">
        <h1 className="title-heading">YK x Updater</h1>
        <div>
          <Nav/>
          <Route exact path="/" component={Home}/>
          <Route path="/about" component={AboutComponent}/>
          <Route path='/explore' component={Explore}/>
        </div>

      <div className="footer">
        Made with Love and React.js, by Yamini Kumawat
      </div>
      </div>
    );
  }
}

export default App;
