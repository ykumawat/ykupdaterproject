export function exploreNeighborhood(searchTerm, location, categories){
  const body = JSON.stringify({
    "searchTerm": searchTerm,
    "location": location,
    "cat": categories
  })
  
  return fetch('https://5ymh1fr7sb.execute-api.us-east-1.amazonaws.com/first/yelp', {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: body
  })
  .then((res) => res.json())
}

export function textLocation(phoneNum, name, address, city, state, zip, url){
  let locationInfo = [name, address, city, state, zip].toString(", ")
  
  const body = JSON.stringify({
    "phoneNum": phoneNum,
    "locationInfo": locationInfo,
    "url": url
  })
  
  return fetch('https://5ymh1fr7sb.execute-api.us-east-1.amazonaws.com/first/yelp', {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: body
  })
  .then((res) => res.json())
}