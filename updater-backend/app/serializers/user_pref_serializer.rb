class UserPrefSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :loc_id, :category_id
end
