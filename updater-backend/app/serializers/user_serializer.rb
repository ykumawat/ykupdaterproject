class UserSerializer < ActiveModel::Serializer
  attributes :id, :username, :zipcode
end
