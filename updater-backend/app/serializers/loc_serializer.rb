class LocSerializer < ActiveModel::Serializer
  attributes :id, :name, :zipcode, :rating
end
