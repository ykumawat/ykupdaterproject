class UserPrefsController < ApplicationController
  
  def index
    @ups = UserPref.all
    render json: @ups, status: 200
  end
  
end
