class UsersController < ApplicationController
  
  def index
    @users = User.all
    render json: @users, status: 200
  end
  
  
  def new
    @user = User.find_or_create_by(user_params)
  end
  
  # def create
  #   user = User.new(user_params)
  #   if user.save
  #     token = encode_token({user_id: user.id})
  #     render json: {user: user, jwt: token}, status: 201
  #   end
  # end
  
  private
  
  def user_params
    params.permit(:username, :zipcode, :private)
  end
end
