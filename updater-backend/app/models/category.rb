class Category < ApplicationRecord
  has_many :locs, through: :user_prefs
  has_many :user_prefs
end
