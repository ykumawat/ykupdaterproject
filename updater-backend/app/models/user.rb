class User < ApplicationRecord
  has_many :user_prefs
  has_many :locs, through: :user_prefs
  has_many :categories, through: :user_prefs
end
