Rails.application.routes.draw do
  resources :categories
  resources :locs
  resources :user_prefs
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
