class AddAddressToLocs < ActiveRecord::Migration[5.1]
  def change
    add_column :locs, :address, :string
  end
end
