class CreateUserPrefs < ActiveRecord::Migration[5.1]
  def change
    create_table :user_prefs do |t|
      t.integer :user_id
      t.integer :loc_id
      t.integer :category_id

      t.timestamps
    end
  end
end
