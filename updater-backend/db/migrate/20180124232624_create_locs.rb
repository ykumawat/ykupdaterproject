class CreateLocs < ActiveRecord::Migration[5.1]
  def change
    create_table :locs do |t|
      t.string :name
      t.string :zipcode
      t.integer :rating
      t.integer :category_id

      t.timestamps
    end
  end
end
